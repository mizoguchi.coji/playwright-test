# playwright-test

GitLab CI 定期実行で Playwright をつかって本番環境での E2E テストを行うテスト。

- GitLab CI の設定ファイル [.gitlab-ci.yml](/.gitlab-ci.yml) が入り口でこれを [GitLab のスケジュールドパイプライン](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) の設定で 1 時間に一回実行するようにしている。
- 実際に起動されるのは package.json に定義されてる scripts から `pnpm test:e2e` を通して `playwright test が GitLab Runner` 上で実行される。
- [tests/techtalkjp.spec.ts](/tests/techtalkjp.spec.ts) に格納されているテストコードが実行される
- 実行結果は html や video として artifact として保存している。異常があったときはこれらをみて確認できる。
