import { startUnleash } from "unleash-client"

export const useFeatureFlag = async () => {
  const unleash = await startUnleash({
    url: "https://gitlab.com/api/v4/feature_flags/unleash/40373296",
    appName: "production",
    instanceId: "TdN9k85yLiARZ4pfHz1b"
  })

  return {
    isEnabled: (name: string, userId?: string) => {
      return unleash.isEnabled(name, { userId })
    }
  }
}
