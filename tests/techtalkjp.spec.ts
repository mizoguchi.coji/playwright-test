import { test, expect } from "@playwright/test"
import { useFeatureFlag } from "./helpers/featureFlag"

test("TechTalkJP website access", async ({ page }, testInfo) => {
  testInfo.snapshotSuffix = ""
  // feature flags が無効の場合はテストをスキップする
  // https://gitlab.com/mizoguchi.coji/playwright-test/-/feature_flags/1/edit
  const feature = await useFeatureFlag()
  test.skip(!feature.isEnabled("test-techtalk-contact", "ACCESS"))

  await page.goto("https://techtalk.jp/")
  await expect(
    page.getByRole("heading", { name: "Technically, It's possible." })
  ).toBeVisible()

  // スクリーンショットのスナップショット比較テスト
  await expect(page).toHaveScreenshot({
    maxDiffPixelRatio: 0.05 // フォントが違いがあるのである程度まで許容する
  })
})

test("TechTalkJP website contact", async ({ page }, testInfo) => {
  testInfo.snapshotSuffix = ""
  // feature flags が無効の場合はテストをスキップする
  // https://gitlab.com/mizoguchi.coji/playwright-test/-/feature_flags/1/edit
  const feature = await useFeatureFlag()
  test.skip(!feature.isEnabled("test-techtalk-contact", "CONTACT"))

  await page.goto("https://www.techtalk.jp/")
  await page.getByPlaceholder("お名前").fill("playwright")
  await page.getByPlaceholder("会社名").fill("Playwright Corporation")
  await page.getByPlaceholder("電話番号").fill("08000000000")
  await page.getByPlaceholder("メール").fill("test@example.com")
  await page
    .getByPlaceholder("メッセージ")
    .fill("here is test message.\nthank you!")

  const requestSnapshotPromise = new Promise((resolve) => {
    page.on("request", async (request) => {
      if (
        request.method() === "POST" &&
        request.url().startsWith("https://www.techtalk.jp/api/contact")
      ) {
        const response = await request.response()
        expect(
          JSON.stringify(
            {
              url: request.url(),
              postData: await request.postDataJSON(),
              response: await response?.json()
            },
            null,
            2
          )
        ).toMatchSnapshot("./contact.json")
        resolve(true)
      }
    })
  })

  await page.getByRole("button", { name: "Let's talk" }).click()

  await requestSnapshotPromise

  await expect(
    await page.getByText(
      "お問い合わせありがとうございます。以下のメッセージを受付けました。お返事をお待ち下さい。"
    )
  ).toBeVisible()
})
