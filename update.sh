#!/bin/sh

npm i -g pnpm
pnpm i --frozen-lockfile
pnpm set store-dir node_modules/.pnpm-store
pnpm playwright test --update-snapshots # スナップショットを更新する